import Controller.AngkaController;
import Model.AngkaModel;
import View.AngkaView;

public class Main {
    public static void main(String[] args) {

        AngkaModel model = new AngkaModel(1, 10);
        AngkaView view = new AngkaView();
        AngkaController controller = new AngkaController(model, view);
        controller.updateView();

    }
}