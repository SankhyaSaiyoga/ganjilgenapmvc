package Model;

public class AngkaModel {

    private int a;

    private int b;

    public AngkaModel(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
