package Controller;

import Model.AngkaModel;
import View.AngkaView;

import java.util.ArrayList;

public class AngkaController {

    private AngkaModel model;

    private AngkaView view;

    public AngkaController(AngkaModel model, AngkaView view) {
        this.model = model;
        this.view = view;
    }

    public void updateView() {
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = model.getA(); i <= model.getB(); i++){
            String status = (i % 2 == 1) ? "ganjil" : "genap";
            numbers.add(" " + i + "\t" + "   " + status);
        }
        view.printAngka(numbers);
    }
}
